'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

const { sanitizeEntity } = require('strapi-utils');

module.exports = {
    async find(ctx) {
        let entities;
    
        ctx.query = {
          ...ctx.query,
          Status: 'Published',
        };
    
        if (ctx.query._q) {
          entities = await strapi.services.video.search(ctx.query);
        } else {
          entities = await strapi.services.video.find(ctx.query);
        }
    
        return entities.map(entity => sanitizeEntity(entity, { model: strapi.models.video }));
      },
};

